module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.com/api/v4/',
  assignees: ['mikebarkmin'],
  baseBranches: ['master'],
  labels: ['renovate'],
  rebaseWhen: 'auto',
  automerge: true,
  automergeType: 'branch',
  semanticCommits: true,
  major: {
    automerge: false
  },
  repositories: [
    "openpatch/assessment-backend",
    "openpatch/activity-backend",
    "openpatch/authentification-backend",
    "openpatch/comment-backend",
    "openpatch/flask-core",
    "openpatch/flask-microservice-base",
    "openpatch/format-backend",
    "openpatch/infrastructure",
    "openpatch/itembank-backend",
    "openpatch/mail-backend",
    "openpatch/media-backend",
    "openpatch/react-microservice-base",
    "openpatch/recorder-backend",
    "openpatch/runner",
    "openpatch/runner-java",
    "openpatch/runner-python",
    "openpatch/templates/backend-flask",
    "openpatch/web-frontend",
    "openpatch/patcher"
  ],
  extends: [
    'config:base',
    ":pinAllExceptPeerDependencies"
  ]
};
